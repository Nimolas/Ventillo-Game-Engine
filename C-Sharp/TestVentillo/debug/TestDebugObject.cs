﻿using NUnit.Framework;
using FluentAssertions;


using Ventillo.Debug;
using Ventillo.System;

using System;
using Moq;
using System.Collections.Generic;
using Ventillo.GameObjects;

namespace TestVentillo.Debug
{
    internal class TestDebugObject
    {
        DebugObject testDebugObject;

        [SetUp]
        public void Setup()
        {
            testDebugObject = new DebugObject(new Vector(0, 0), new MinMax(new Vector(-400, -400), new Vector(400, 400)));
        }

        //[Test]
        //public void TestConstructor_CallsSetsDrawObject_WhenInitialised()
        //{
        //    var min = 0;
        //    var max = 800;

        //    double xMidPosition = 400;
        //    double yMidPosition = 400;


        //    List<DrawObject> ExpectedDrawObjects = new List<DrawObject>()
        //    {
        //        new DrawObject(
        //            new List<Vector>(){
        //                new Vector(min - 1, min),
        //                new Vector(min + 1, min),
        //                new Vector(min + 1, max),
        //                new Vector(min - 1, max),
        //            },
        //            new Colour(209,49,17,155),
        //            new Colour()
        //        ),
        //        new DrawObject(
        //            new List<Vector>(){
        //                new Vector(min, min - 1),
        //                new Vector(max, min - 1),
        //                new Vector(max, min + 1),
        //                new Vector(min, min + 1),
        //            },
        //            new Colour(209,49,17,155),
        //            new Colour()
        //        ),
        //        new DrawObject(
        //            new List<Vector>(){
        //                new Vector(max + 1, min),
        //                new Vector(max + 1, max),
        //                new Vector(max - 1, max),
        //                new Vector(max - 1, min),
        //            },
        //            new Colour(209,49,17,155),
        //            new Colour()
        //        ),
        //        new DrawObject(
        //            new List<Vector>(){
        //                new Vector(max, max - 1),
        //                new Vector(max, max + 1),
        //                new Vector(min, max + 1),
        //                new Vector(min, max - 1),
        //                },
        //            new Colour(209,49,17,155),
        //            new Colour()
        //        ),
        //        new DrawObject(
        //            new List<Vector>(){
        //                new Vector(xMidPosition - 1, min),
        //                new Vector(xMidPosition + 1, min),
        //                new Vector(xMidPosition + 1, max),
        //                new Vector(xMidPosition - 1, max),
        //                },
        //            new Colour(209,49,17,155),
        //            new Colour()
        //        ),
        //        new DrawObject(
        //            new List<Vector>(){
        //                new Vector(min, yMidPosition - 1),
        //                new Vector(max, yMidPosition - 1),
        //                new Vector(max, yMidPosition + 1),
        //                new Vector(min, yMidPosition + 1),
        //                },
        //            new Colour(209,49,17,155),
        //            new Colour()
        //        ),
        //    };

        //    var mockDebugObject = new Mock<DebugObject>();
        //    mockDebugObject.Protected().Setup("SetDrawObject", ItExpr.IsAny<List<DrawObject>>()).Verifiable();

        //    mockDebugObject.Verify();
        //}

        [Test]

        public void TestGetFPS_ReturnsCurrentFPSValue()
        {
            double beforeTimestamp = 16;
            double afterTimestamp = 32;

            testDebugObject.Update(beforeTimestamp);
            testDebugObject.Update(afterTimestamp);

            var expectedFPSValue = 1000 / (afterTimestamp - beforeTimestamp);

            Assert.AreEqual(expectedFPSValue, testDebugObject.GetFPS());
        }

        [Test]
        public void TestDraw_CallsSubsequentDrawMethods()
        {
            var mockDebug = new Mock<DebugObject>(new Vector(0, 0), new MinMax(new Vector(0, 0), new Vector(400, 400)));
            mockDebug
                .Setup(mock => mock.Draw())
                .Callback(() =>
                {
                    mockDebug.Object.DrawByLine(new List<DrawObject>());
                    mockDebug.Object.DrawByText("Hello Test", new Vector(0, 0), new Colour(0, 0, 0, 0), 0);
                });

            mockDebug.Object.Draw();

            mockDebug
                .Verify(mock => mock.DrawByLine(new List<DrawObject>() { }), Times.Once);

            mockDebug
                .Verify(mock => mock.DrawByText(It.IsAny<string>(), It.IsAny<Vector>(), It.IsAny<Colour>(), It.IsAny<uint>()), Times.Once);
        }
    }
}
