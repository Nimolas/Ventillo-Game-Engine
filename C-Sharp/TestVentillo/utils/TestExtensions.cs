﻿using NUnit.Framework;
using FluentAssertions;
using Ventillo.System;
using Ventillo.Utils;

using SFML.System;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;

namespace TestVentillo.Utils
{
    public class TestExtensions
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void TestConvertToSFMLVector_ReturnsSFMLVector_FromVentilloVector()
        {
            new Vector(10, 10).ConvertToSFMLVector().Should().BeEquivalentTo(new Vector2f(10, 10));
        }

        [Test]
        public void TestGetTimeInMilliseconds_ReturnsMillisecondsFromDateTimeObject()
        {
            //Date I made the test
            new DateTime(2022, 02, 27).GetTimeInMilliseconds().Equals(63781689600000);
        }

        [Test]
        public void TestToObjectString_ConvertsObjectsToJSONString()
        {
            var jsonTextWriter = new JsonTextWriter(new StreamWriter("TestPath")); //Path is just there so I can make a stream writer for the indents
            jsonTextWriter.Formatting = Formatting.Indented;
            var jsonIndentation = new string(jsonTextWriter.IndentChar, jsonTextWriter.Indentation);
            var newLine = Environment.NewLine;

            var jsonValue = $"{{{newLine}{jsonIndentation}\"Hello\": \"World\"{newLine}}}";
            new Dictionary<string, string>() { { "Hello", "World" } }.ToObjectString().Should().BeEquivalentTo(jsonValue);
        }
    }
}
