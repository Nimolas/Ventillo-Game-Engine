﻿using System.Collections.Generic;
using Ventillo;
using Ventillo.GameObjects;
using Ventillo.System;

namespace Example.GameObjects
{
    class Square : GameObject
    {
        public Square(Vector position) : base(position)
        {
            var drawObject = new List<DrawObject>(){
                new DrawObject(
                    new List<Vector>()
                    {
                        new Vector(-50, -50),
                        new Vector(50, -50),
                        new Vector(50, 50),
                        new Vector(-50, 50),
                    },
                    new Colour(110, 34, 199, 255),
                    new Colour()
                )
            };

            SetDrawObject(drawObject);
        }

        public override void Update(List<GameObject> gameObjects)
        {
            Engine.Keys.ForEach(key =>
            {
                switch (key)
                {
                    case "W":
                        Position.Translate(new Vector(0, -10));
                        break;
                    case "A":
                        Position.Translate(new Vector(-10, 0));
                        break;
                    case "S":
                        Position.Translate(new Vector(0, 10));
                        break;
                    case "D":
                        Position.Translate(new Vector(10, 0));
                        break;
                }
            });
        }
    }
}
