﻿using Example.Game;
using System;
using Ventillo;

namespace Example
{
    class Program
    {
        static Engine engine = new(60);
        static ExampleGame exampleGame = new();
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            engine.SetGame(exampleGame);
            engine.Start();
        }
    }
}
