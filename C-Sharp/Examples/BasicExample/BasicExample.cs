﻿using Example.Scenes;
using System.Collections.Generic;
using Ventillo;
using Ventillo.Interfaces;

namespace Example.Game
{
    internal class ExampleGame : IGame
    {
        public List<IScene> Scenes { get; set; } = new();
        public IScene CurrentScene { get; set; }

        internal ExampleGame()
        {
            Engine.SetWindowTitle("Example Game");
            Scenes.Add(new BasicScene());
        }

        ~ExampleGame()
        {
            Scenes.Clear();
        }

        public void Update()
        {
            CurrentScene.Update();
        }

        public void Draw()
        {
            CurrentScene.Draw();
        }

    }
}
