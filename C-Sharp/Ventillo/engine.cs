﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Ventillo.Debug;
using Ventillo.Exceptions;
using Ventillo.Interfaces;
using Ventillo.System;
using Ventillo.Utils;

namespace Ventillo
{
    internal class CoRoutine
    {
        public string Type { get; set; }
        public IEnumerator Fn { get; set; }
        public CoRoutine(string type, IEnumerator fn)
        {
            this.Type = type;
            this.Fn = fn;
        }

    }
    public class Engine
    {
        static internal readonly RenderWindow window = new(new VideoMode(1280, 720), "Ventillo Engine");
        static public List<string> Keys { get; set; }
        static public List<Vector> MouseClickPositions { get; set; }
        static public double FPS { get; set; }
        private static readonly List<CoRoutine> coroutines = new();
        static public MinMax PlayableArea { get; set; }
        static private IScene NextScene { get; set; } = null;
        static public Logger Logger { get; set; }
        static internal readonly string contentFilePath = "contentFiles/any/any";
        private bool debug = false;
        private bool runGame = false;
        private DebugObject debugObject;
        private IGame game;
        private readonly uint fpsLimit = 0;

        public Engine(uint FPSLimit)
        {
            fpsLimit = FPSLimit;

            SetupEngine();
            SetupEvents();
        }

        public static RenderWindow GetWindow()
        {
            return window;
        }

        public void SetGame(IGame game)
        {
            runGame = true;
            this.game = game;
        }

        public void SetScene(IScene scene)
        {
            runGame = true;
            game.CurrentScene = scene;
        }

        public void Start(IScene scene = null)
        {
            if (scene == null)
            {
                try
                {
                    scene = game.Scenes.ElementAt(0);
                }
                catch (Exception error)
                {
                    Logger.Error("No scene found in the game. Please add a scene to the scene collection, or provide one to the start function", error, game.ToObjectString());
                    throw new MissingSceneException("No scene found in the game. Please add a scene to the scene collection, or provide one to the start function");
                }
            }

            game.CurrentScene = scene;

            Logger.Info("Engine started");

            StartInternalCoRoutine(CheckForNextScene());
            StartInternalCoRoutine(CheckForNullObjects());

            GameLoop();
        }

        static void RemoveGameCoRoutines()
        {
            coroutines.FindAll(coRoutine => coRoutine.Type == "Game").ForEach(gameCoroutine =>
            {
                coroutines.Remove(gameCoroutine);
            });
        }

        public void Stop()
        {
            runGame = false;

            RemoveGameCoRoutines();

            Keys.Clear();

            game.CurrentScene = null;
        }

        static public void SwitchScene(IScene scene)
        {
            NextScene = scene;
        }

        static void StartInternalCoRoutine(IEnumerator coroutine)
        {
            coroutines.Add(new CoRoutine("Engine", coroutine));
        }

        public static void StartCoRoutine(IEnumerator coroutine)
        {
            coroutines.Add(new CoRoutine("Game", coroutine));
        }

        static public double GetWindowWidth()
        {
            return window.Size.X;
        }

        static public double GetWindowHeight()
        {
            return window.Size.Y;
        }

        static IEnumerator InternalWaitForSeconds(double seconds)
        {
            var secsInMilli = seconds * 1000;
            var now = new DateTime().GetTimeInMilliseconds();

            while (new DateTime().GetTimeInMilliseconds() - now < secsInMilli)
            {
                yield return null;
            }
        }

        static public void WaitForSeconds(double seconds)
        {
            var wait = InternalWaitForSeconds(seconds);
            while (wait.MoveNext())
            { /*Empty so we just execute until the time set has passed*/ }
        }

        IEnumerator CheckForNextScene()
        {
            while (window.IsOpen)
            {
                if (NextScene != null)
                {
                    Stop();
                    SetScene(NextScene);
                    NextScene = null;
                }

                yield return null;
            }
        }

        IEnumerator CheckForNullObjects()
        {
            while (window.IsOpen)
            {
                game.CurrentScene.GameObjects.FindAll(gameObject => gameObject == null).ForEach(gameObject => game.CurrentScene.GameObjects.Remove(gameObject));

                yield return null;
            }
        }

        void Draw()
        {
            ClearScreen();
            game.Draw();
            DrawDebug();
        }

        void DrawDebug()
        {
            if (debug)
            {
                game.CurrentScene.GameObjects.ForEach(gameObject => debugObject.DrawObjectBounds(gameObject));
                debugObject.Draw();
            }
        }

        void Update()
        {
            game.Update();

            CheckDebug();

            debugObject.Update(new DateTime().GetTimeInMilliseconds());

            Keys.Clear();

            MouseClickPositions.Clear();
        }

        static void ExecuteCoRoutines()
        {
            coroutines.FindAll(corotuine => !corotuine.Fn.MoveNext()).ForEach(coroutine => coroutines.Remove(coroutine));
        }

        void GameLoop()
        {
            while (window.IsOpen)
            {
                if (runGame)
                {
                    window.DispatchEvents();

                    Update();
                    ExecuteCoRoutines();
                    Draw();

                    window.Display();
                }
            }
        }

        void CheckDebug()
        {
            Keys.ForEach(key =>
            {
                switch (key)
                {
                    case "Q":
                        debug = !debug;
                        break;
                }
            });
        }

        void SetupEvents()
        {
            window.Closed += (object sender, EventArgs e) =>
            {
                window.Close();
                runGame = false;
            };

            window.KeyPressed += (object sender, KeyEventArgs e) =>
            {
                Keys.Add(e.Code.ToString());
            };

            window.MouseButtonPressed += (object sender, MouseButtonEventArgs e) =>
            {
                MouseClickPositions.Add(new Vector(e.X, e.Y));
            };
        }

        public static void SetWindowTitle(string title)
        {
            window.SetTitle(title);
        }

        void SetupEngine()
        {
            window.SetFramerateLimit(fpsLimit);

            Logger = new Logger(LoggerLevels.DEBUG);

            Keys = new();

            MouseClickPositions = new();

            PlayableArea = new MinMax(
                new Vector(GetWindowWidth() * 0.15, GetWindowHeight() * 0.10),
                new Vector(GetWindowWidth() * 0.85, GetWindowHeight() * 0.9)
            );

            debugObject = new(new Vector(0, 0), PlayableArea);
        }

        static void ClearScreen()
        {
            window.Clear();
        }
    }
}

