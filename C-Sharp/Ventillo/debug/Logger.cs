using System;
using Ventillo.Utils;
using Ventillo.Debug.Interfaces;

namespace Ventillo.Debug
{
    public enum LoggerLevels
    {
        DEBUG,
        INFO,
        WARN,
        ERROR
    }
    public class Logger : ILogger
    {
        readonly LoggerLevels level;
        public Logger(LoggerLevels level)
        {
            this.level = level;
        }

        public void Debug(string message, object data = null)
        {
            if (level <= LoggerLevels.DEBUG)
            {
                Console.ForegroundColor = ConsoleColor.Gray;

                if (data == null)
                    Console.WriteLine($"DEBUG: {message}");
                else
                    Console.WriteLine($"DEBUG: {message}, {data.ToObjectString()}");
            }
        }

        public void Info(string message, object data = null)
        {
            if (level <= LoggerLevels.INFO)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;

                if (data == null)
                    Console.WriteLine($"INFO: {message}");
                else
                    Console.WriteLine($"INFO: {message}, {data.ToObjectString()}");
            }
        }

        public void Warn(string message, object data = null)
        {
            if (level <= LoggerLevels.WARN)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;

                if (data == null)
                    Console.WriteLine($"WARN: {message}");
                else
                    Console.WriteLine($"WARN: {message}, {data.ToObjectString()}");
            }
        }

        public void Error(string message, Exception error, object data = null)
        {
            if (level <= LoggerLevels.ERROR)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                var consoleMessage = $"ERROR: {message}, {error}";

                if (data != null)
                    consoleMessage += $" - {data.ToObjectString()}";

                Console.WriteLine(consoleMessage);
            }
        }
    }
}