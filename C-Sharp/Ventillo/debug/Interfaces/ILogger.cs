﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ventillo.Debug.Interfaces
{
    public interface ILogger
    {
        void Debug(string message, object data = null);
        void Info(string message, object data = null);
        void Warn(string message, object data = null);
        void Error(string message, Exception error, object data = null);
    }
}
