using System;
using Newtonsoft.Json;

using SFML.System;

using Ventillo.System;

namespace Ventillo.Utils
{
    public static class Extensions
    {
        internal static Vector2f ConvertToSFMLVector(this Vector Position)
        {
            return new Vector2f((float)Position.X, (float)Position.Y);
        }

        public static double GetTimeInMilliseconds(this DateTime newDateTime)
        {
            return (double)newDateTime.Ticks / TimeSpan.TicksPerMillisecond;
        }

        public static string ToObjectString(this object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented);
        }
    }
}