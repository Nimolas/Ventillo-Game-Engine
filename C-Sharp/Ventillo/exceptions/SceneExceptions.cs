﻿using System;
using System.Runtime.Serialization;

namespace Ventillo.Exceptions
{

    [Serializable]
    public class MissingSceneException : VentilloException
    {

        public MissingSceneException(string message)
            : base(message)
        {

        }

        protected MissingSceneException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            // MUST call through to the base class to let it save its own state
            base.GetObjectData(info, context);
        }
    }
}
