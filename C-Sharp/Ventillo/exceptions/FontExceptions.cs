﻿using System;
using System.Runtime.Serialization;

namespace Ventillo.Exceptions
{

    [Serializable]
    public class MissingFontException : VentilloException
    {
        private string FontPath { get; }

        public MissingFontException(string message, string fontPath)
            : base(message)
        {
            FontPath = fontPath;
        }

        protected MissingFontException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            FontPath = info.GetString("ResourceName");
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue("ResourceName", this.FontPath);

            // MUST call through to the base class to let it save its own state
            base.GetObjectData(info, context);
        }
    }
}
