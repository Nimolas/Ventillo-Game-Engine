﻿using System;

using System.Runtime.Serialization;

namespace Ventillo.Exceptions
{
    [Serializable]
    public class VentilloException : Exception
    {
        public VentilloException(string message) : base(message) { }

        // Without this constructor, deserialization will fail
        protected VentilloException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
