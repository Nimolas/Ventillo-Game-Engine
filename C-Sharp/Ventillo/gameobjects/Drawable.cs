﻿using SFML.Graphics;
using SFML.System;

using System;
using System.Collections.Generic;
using System.Linq;

using Ventillo.Exceptions;
using Ventillo.GameObjects.Interfaces;
using Ventillo.System;
using Ventillo.Utils;

namespace Ventillo.GameObjects
{
    public class Drawable : Transform, IDrawable
    {
        public List<DrawObject> DrawObjects { get; set; } = new List<DrawObject>();
        protected Font font = null;

        protected void SetDrawObject(List<DrawObject> DrawObjects)
        {
            this.DrawObjects = DrawObjects;
            //GetObjectBounds(); Broken until Physics is implemented
        }

        public virtual void Draw()
        {
            DrawByLine(DrawObjects);
        }

        public void DrawAPixel(Vector Position)
        {
            var TempShape = new ConvexShape();
            try
            {
                TempShape.Position = Position.ConvertToSFMLVector();
                TempShape.Origin = TempShape.Position;
                TempShape.SetPointCount(4);

                TempShape.SetPoint(0, new Vector2f(-0.5f, 0.5f));
                TempShape.SetPoint(1, new Vector2f(0.5f, 0.5f));
                TempShape.SetPoint(2, new Vector2f(0.5f, -0.5f));
                TempShape.SetPoint(3, new Vector2f(-0.5f, -0.5f));

                Engine.window.Draw(TempShape);
            }
            catch (Exception error)
            {
                Engine.Logger.Error("Failed to draw a pixel", error, TempShape);
                throw;
            }
        }

        protected void DrawByLine()
        {
            DrawByLine(DrawObjects);
        }

        public virtual void DrawByLine(List<DrawObject> drawObjects)
        {
            try
            {
                drawObjects.ForEach(drawable =>
                {
                    var TempShape = new ConvexShape()
                    {
                        Position = Position.ConvertToSFMLVector()
                    };

                    TempShape.SetPointCount((uint)(drawable.DrawPoints.Count));

                    for (var drawPointIndex = 0; drawPointIndex < drawable.DrawPoints.Count; drawPointIndex++)
                    {
                        var drawPoint = drawable.DrawPoints.ElementAt(drawPointIndex);
                        TempShape.SetPoint((uint)drawPointIndex, drawPoint.ConvertToSFMLVector());
                    }

                    SetDrawModes(TempShape, drawable.StrokeColour, drawable.FillColour);
                    Engine.window.Draw(TempShape);
                });
            }
            catch (Exception error)
            {
                Engine.Logger.Error("Failed to draw by line", error);
                throw;
            }
        }

        public Font LoadFont(string fontPath)
        {
            try
            {
                return new Font(fontPath);
            }
            catch (Exception error)
            {
                Engine.Logger.Error($"Failed to load font {fontPath}", error, new { message = error.Message, stackTrace = error.StackTrace, source = error.Source, targetSite = error.TargetSite });
                throw new MissingFontException($"Failed to load font ${fontPath}. Exception: {error}", fontPath);
            }
        }

        private Font LoadDebugText()
        {
            try
            {
                return LoadFont($"{Engine.contentFilePath}/debug/fonts/GIL_____.ttf");
            }
            catch (Exception)
            {
                Engine.Logger.Warn("Failed to load debug font file. Attempting second method");
            }

            try
            {
                return LoadFont("debug/fonts/GIL_____.ttf");
            }
            catch (Exception error)
            {
                Engine.Logger.Error("Second attempt failed. Failed to load font. Throwing", error);
                throw;
            }
        }

        public virtual void DrawByText(string text, Vector position, Colour colour, uint fontSize = 14)
        {
            try
            {
                if (font == null)
                    font = LoadDebugText();

                var SFMLtext = new Text(text, font, fontSize)
                {
                    Position = position.ConvertToSFMLVector(),

                    // set the color
                    FillColor = colour.GetSFMLColor(),

                    // set the text style
                    Style = Text.Styles.Regular
                };

                Engine.window.Draw(SFMLtext);
            }
            catch (Exception error)
            {
                Engine.Logger.Error("Failed to draw text", error);
                throw;
            }
        }

        protected void DrawByPixel()
        {
            DrawByPixel(DrawObjects);
        }

        public void DrawByPixel(List<DrawObject> drawObjects)
        {
            try
            {
                DrawObjects.ForEach(drawable =>
                {
                    drawable.DrawPoints.ForEach(drawPoint =>
                    {
                        var TempShape = new ConvexShape
                        {
                            Position = Position.ConvertToSFMLVector()
                        };

                        TempShape.Origin = TempShape.Position;
                        TempShape.SetPointCount(4);

                        TempShape.SetPoint(0, new Vector(drawPoint.X - 0.5f, drawPoint.Y + 0.5f).ConvertToSFMLVector());
                        TempShape.SetPoint(1, new Vector(drawPoint.X + 0.5f, drawPoint.Y + 0.5f).ConvertToSFMLVector());
                        TempShape.SetPoint(2, new Vector(drawPoint.X + 0.5f, drawPoint.Y - 0.5f).ConvertToSFMLVector());
                        TempShape.SetPoint(3, new Vector(drawPoint.X - 0.5f, drawPoint.Y - 0.5f).ConvertToSFMLVector());

                        SetDrawModes(TempShape, drawable.StrokeColour, drawable.FillColour);
                        Engine.window.Draw(TempShape);
                    });
                });
            }
            catch (Exception error)
            {
                Engine.Logger.Error("Failed to draw by pixel from draw object", error);
                throw;
            }
        }

        protected void DrawByCircle()
        {
            DrawByCircle(DrawObjects);
        }

        public void DrawByCircle(List<DrawObject> drawObjects)
        {
            try
            {
                DrawObjects.ForEach(drawable =>
                {
                    drawable.DrawPoints.ForEach(drawPoint =>
                    {
                        var TempShape = new CircleShape((float)(drawPoint.X))
                        {
                            Position = Position.ConvertToSFMLVector(),
                            Origin = new Vector(
                                drawPoint.X,
                                drawPoint.Y
                            ).ConvertToSFMLVector()
                        };

                        SetDrawModes(TempShape, drawable.StrokeColour, drawable.FillColour);
                        Engine.window.Draw(TempShape);
                    });
                });
            }
            catch (Exception error)
            {
                Engine.Logger.Error("Failed to draw by circle", error);
                throw;
            }
        }

        public void SetDrawModes(Shape TempShape, Colour StrokeStyle, Colour FillStyle)
        {
            try
            {
                TempShape.OutlineColor = StrokeStyle.GetSFMLColor();
                TempShape.FillColor = FillStyle.GetSFMLColor();
            }
            catch (Exception error)
            {
                Engine.Logger.Error("Failed to set draw mode", error);
                throw;
            }
        }
    }
}
