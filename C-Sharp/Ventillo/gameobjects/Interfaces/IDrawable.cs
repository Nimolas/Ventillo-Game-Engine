﻿using SFML.Graphics;
using System.Collections.Generic;
using Ventillo.System;

namespace Ventillo.GameObjects.Interfaces
{
    internal interface IDrawable
    {
        List<DrawObject> DrawObjects { get; set; }
        void Draw();
        void DrawAPixel(Vector Position);
        void DrawByLine(List<DrawObject> drawObjects);
        void DrawByText(string text, Vector position, Colour colour, uint fontSize = 14);
        void DrawByPixel(List<DrawObject> drawObjects);
        void DrawByCircle(List<DrawObject> drawObjects);
        Font LoadFont(string fontPath);
        void SetDrawModes(Shape TempShape, Colour StrokeStyle, Colour FillStyle);

    }
}
