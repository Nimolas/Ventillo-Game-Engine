﻿
using Ventillo.GameObjects.Interfaces;
using Ventillo.System;

namespace Ventillo.GameObjects
{
    public class Transform : ITransform
    {
        public Vector Position { get; set; } = null;

        public Vector ToGlobalCoords(Vector localVector)
        {
            return new Vector(Position.X + localVector.X, Position.Y + localVector.Y);
        }

        public Vector ToLocalCoords(Vector globalVector)
        {
            return new Vector(globalVector.X - Position.X, globalVector.Y - Position.Y);
        }
    }
}
