using System;
using System.Collections.Generic;
using System.Linq;
using Ventillo.GameObjects.Interfaces;
using Ventillo.System;

namespace Ventillo.GameObjects
{
    public class GameObject : Drawable, IGameObject
    {
        public bool ToDelete { get; set; } = false;
        public MinMax MinMax { get; set; }

        public GameObject(Vector position)
        {
            this.Position = position;
        }

        ~GameObject()
        {
            DrawObjects = null;
            Position = null;
            MinMax = null;
        }

        public virtual void CheckDelete(List<GameObject> GameObjects)
        {

        }

        public virtual void Update(List<GameObject> GameObjects)
        {

        }

        private bool DetectAABBCollision(GameObject other)
        {
            try
            {
                var (MinX, MaxX, MinY, MaxY) = (false, false, false, false);

                Vector OtherMinGlobal = other.ToGlobalCoords(other.MinMax.Min);
                Vector OtherMaxGlobal = other.ToGlobalCoords(other.MinMax.Max);

                Vector ThisMinGlobal = ToGlobalCoords(MinMax.Min);
                Vector ThisMaxGlobal = ToGlobalCoords(MinMax.Max);

                if (OtherMinGlobal.X > ThisMinGlobal.X && OtherMinGlobal.X < ThisMaxGlobal.X)
                    MinX = true;
                if (OtherMaxGlobal.X > ThisMinGlobal.X && OtherMaxGlobal.X < ThisMaxGlobal.X)
                    MaxX = true;


                if (OtherMinGlobal.Y > ThisMinGlobal.Y && OtherMinGlobal.Y < ThisMaxGlobal.Y)
                    MinY = true;
                if (OtherMaxGlobal.Y > ThisMinGlobal.Y && OtherMaxGlobal.Y < ThisMaxGlobal.Y)
                    MaxY = true;

                return (MinX || MaxX) && (MinY || MaxY); //If horizontal point and vertical point overlapping, doesn't matter which ones or if multiple of either
            }
            catch (Exception error)
            {
                Engine.Logger.Error("Failed to detect AABBCollision", error, other);
                throw;
            }
        }

        private void AssignIndividualObjectBounds()
        {
            try
            {
                DrawObjects.ForEach(drawable =>
                {
                    var Min = new Vector(double.MaxValue, double.MaxValue);
                    var Max = new Vector(double.MinValue, double.MinValue);

                    drawable.DrawPoints.ForEach(drawPoint =>
                    {
                        if (drawPoint.X < Min.X)
                            Min = new Vector(drawPoint.X, Min.Y);
                        if (drawPoint.Y < Min.Y)
                            Min = new Vector(Min.X, drawPoint.Y);


                        if (drawPoint.X > Max.X)
                            Max = new Vector(drawPoint.X, Max.Y);
                        if (drawPoint.Y > Max.Y)
                            Max = new Vector(Max.X, drawPoint.Y);
                    });

                    var minMax = new MinMax(Min, Max);

                    var index = DrawObjects.IndexOf(drawable);
                    var OldMinMax = DrawObjects.ElementAt(index);
                    var NewMinMax = OldMinMax;

                    NewMinMax.MinMax = minMax;
                    DrawObjects[index] = NewMinMax; //Assign each individual part of a drawObject's minMax
                });
            }
            catch (Exception error)
            {
                Engine.Logger.Error("Failed to assign individual object bounds", error);
                throw;
            }
        }

        private void AssignTotalObjectBounds()
        {
            try
            {
                var Min = new Vector(double.MaxValue, double.MaxValue);
                var Max = new Vector(double.MinValue, double.MinValue);

                DrawObjects.ForEach(drawable =>
                {
                    if (drawable.MinMax.Max.X > Max.X)
                        Max = new Vector(drawable.MinMax.Max.X, Max.Y);
                    if (drawable.MinMax.Max.Y > Max.Y)
                        Max = new Vector(Max.X, drawable.MinMax.Max.Y);
                    if (drawable.MinMax.Min.X < Min.X)
                        Min = new Vector(drawable.MinMax.Min.X, Min.Y);
                    if (drawable.MinMax.Min.Y < Min.Y)
                        Min = new Vector(Min.X, drawable.MinMax.Min.Y);
                });

                var minMax = new MinMax(Min, Max);

                this.MinMax = minMax; //Assign overall box of the entire object
            }
            catch (Exception error)
            {
                Engine.Logger.Error("Failed to assign total object bounds", error);
                throw;
            }
        }

        private void GetObjectBounds() //Used to find the AABB (Axis-Aligned Bounding Box). Basically the basic box around the object to be used as primitive hit detection
        {
            AssignIndividualObjectBounds();
            AssignTotalObjectBounds();
        }

        public double GetWidth()
        {
            return MinMax.Max.X - MinMax.Min.X;
        }

        public double GetHeight()
        {
            return MinMax.Max.Y - MinMax.Min.Y;
        }
    }

}
