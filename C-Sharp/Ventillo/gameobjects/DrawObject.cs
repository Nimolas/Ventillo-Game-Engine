﻿using System.Collections.Generic;

using Ventillo.GameObjects.Interfaces;
using Ventillo.System;

namespace Ventillo.GameObjects
{
    public class DrawObject : IDrawObject
    {
        public List<Vector> DrawPoints { get; set; }
        public Colour FillColour { get; set; }
        public Colour StrokeColour { get; set; }
        public MinMax? MinMax { get; set; }
        public string? Text { get; set; }

        public DrawObject(List<Vector> drawPoints, Colour fillColor, Colour strokeColour, MinMax minMax = null, string text = "")
        {
            DrawPoints = drawPoints;
            FillColour = fillColor;
            StrokeColour = strokeColour;
            MinMax = minMax;
            Text = text;
        }
    }
}
