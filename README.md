[![pipeline status](https://gitlab.com/Nimolas/Ventillo-Game-Engine/badges/main/pipeline.svg)](https://gitlab.com/Nimolas/Ventillo-Game-Engine)
[![coverage report](https://gitlab.com/Nimolas/Ventillo-Game-Engine/badges/main/coverage.svg)](https://gitlab.com/Nimolas/Ventillo-Game-Engine)

# Ventillo-Game-Engine
A game engine made in my spare time, written in multiple languages
